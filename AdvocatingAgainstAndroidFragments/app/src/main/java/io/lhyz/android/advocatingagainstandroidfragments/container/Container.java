package io.lhyz.android.advocatingagainstandroidfragments.container;

/**
 * 这个简单容器展示一项内容，并处理回退操作
 */
public interface Container {
    void showItem(String item);

    boolean onBackPressed();
}
