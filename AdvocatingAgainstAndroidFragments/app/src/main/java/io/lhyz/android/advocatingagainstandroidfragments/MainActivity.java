package io.lhyz.android.advocatingagainstandroidfragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import io.lhyz.android.advocatingagainstandroidfragments.container.Container;

/**
 * FragmentBasic项目实现的是一个展示列表articles的list界面和显示具体article界面的业务流程
 * <p/>
 * 所以要管理两个具体的view窗口，因此使用的viewGroup需要两个
 * <p/>
 * 1.列表界面(ItemListView);
 * 2.详细界面(DetailView)）
 */
public class MainActivity extends AppCompatActivity {

    private Container mContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContainer = (Container) findViewById(R.id.container);
    }

    public Container getContainer() {
        return mContainer;
    }

    @Override
    public void onBackPressed() {
        boolean handled = mContainer.onBackPressed();
        if (!handled) {
            finish();
        }
    }
}
