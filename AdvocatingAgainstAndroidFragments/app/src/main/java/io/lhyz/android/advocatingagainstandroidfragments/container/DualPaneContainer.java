package io.lhyz.android.advocatingagainstandroidfragments.container;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import io.lhyz.android.advocatingagainstandroidfragments.view.DetailView;

/**
 * 双重窗口container
 */
public class DualPaneContainer extends LinearLayout implements Container {
    private DetailView mDetailView;

    public DualPaneContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDetailView = (DetailView) getChildAt(1);
    }

    @Override
    public void showItem(String item) {
        mDetailView.setItem(item);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
