package io.lhyz.android.advocatingagainstandroidfragments.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import io.lhyz.android.advocatingagainstandroidfragments.MainActivity;
import io.lhyz.android.advocatingagainstandroidfragments.container.Container;
import io.lhyz.android.advocatingagainstandroidfragments.data.Ipsum;

public class ItemListView extends ListView {
    public ItemListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, Ipsum.Headlines);
        setAdapter(adapter);

        setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = Ipsum.Articles[position];
                MainActivity activity = (MainActivity) getContext();
                Container container = activity.getContainer();
                container.showItem(item);
            }
        });
    }
}
