package io.lhyz.android.advocatingagainstandroidfragments.presenter;

import android.util.Log;

import io.lhyz.android.advocatingagainstandroidfragments.view.DetailView;

/**
 * 将detailView的业务逻辑剥离并放在此presenter中处理
 */
public class DetailPresenter {
    private DetailView mDetailView;

    public void setView(DetailView detailView) {
        mDetailView = detailView;
    }

    public void buttonClicked() {
        Log.e("TAG", "buttonClicked");
        /**
         * 这里可以对detailView进行ui操作显示相关结果,
         * 下面的断言现在没用，知识为了消除IDE警告
         */
        assert mDetailView != null;
    }
}
