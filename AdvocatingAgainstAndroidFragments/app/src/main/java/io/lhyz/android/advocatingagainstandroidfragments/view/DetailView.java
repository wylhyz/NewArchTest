package io.lhyz.android.advocatingagainstandroidfragments.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.lhyz.android.advocatingagainstandroidfragments.R;
import io.lhyz.android.advocatingagainstandroidfragments.presenter.DetailPresenter;

public class DetailView extends LinearLayout {
    /**
     * 这个界面包含的子view
     */
    TextView mTextView;
    Button mButton;
    /**
     * 这个界面的业务逻辑托管
     */
    DetailPresenter mPresenter;

    /**
     * 两个参数的构造方法是在xml中定义ui的被调用方法，这个必需
     */
    public DetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPresenter = new DetailPresenter();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mPresenter.setView(this);
        mTextView = (TextView) findViewById(R.id.text);
        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.buttonClicked();
            }
        });
    }

    /**
     * 设置要显示的内容，提供对外调用的接口
     */
    public void setItem(String item) {
        mTextView.setText(item);
    }
}
