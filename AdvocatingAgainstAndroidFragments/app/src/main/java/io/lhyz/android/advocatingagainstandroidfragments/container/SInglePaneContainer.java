package io.lhyz.android.advocatingagainstandroidfragments.container;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import io.lhyz.android.advocatingagainstandroidfragments.R;
import io.lhyz.android.advocatingagainstandroidfragments.view.DetailView;
import io.lhyz.android.advocatingagainstandroidfragments.view.ItemListView;

/**
 * 单窗口container
 * <p/>
 * （这里我们要明确的一个问题是，任何时刻这个container都有一个子view）
 */
public class SinglePaneContainer extends FrameLayout implements Container {
    private ItemListView mItemListView;

    public SinglePaneContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mItemListView = (ItemListView) getChildAt(0);
    }

    /**
     * 单窗口模式下，每次调用此方法说明我们想显示详细信息，
     * 所以逻辑就是如果listview还存在，remove它，然后换成detailview，
     * 否则直接调用detailview的方法
     */
    @Override
    public void showItem(String item) {
        if (listViewAttached()) {
            removeViewAt(0);
            View.inflate(getContext(), R.layout.detail, this);
        }
        DetailView detailView = (DetailView) getChildAt(0);
        detailView.setItem(item);
    }

    @Override
    public boolean onBackPressed() {
        if (!listViewAttached()) {
            /**
             * 如果当前子view不是itemListView，那么就是detailView，所以换成itemListView，
             * 然后返回true，表明已处理了后退事件（做了一个拦截）
             * **/
            removeViewAt(0);
            addView(mItemListView);
            return true;
        }
        return false;
    }

    /**
     * 判断是否view还在当前container中，相当于原来用fragmentManager
     * 来findFragment然后判断是否获取到的fragment为空等等，不过更简单一点
     */
    private boolean listViewAttached() {
        return mItemListView.getParent() != null;
    }
}
